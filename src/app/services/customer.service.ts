import { customers, MockDataService } from "./../shared/mock";
import { ICustomer } from "./../shared/interfaces";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CustomerService {
  constructor(private mockDataService: MockDataService) {}

  getCustomers(): Observable<ICustomer[]> {
    return this.mockDataService.getCustomers();
  }
}
