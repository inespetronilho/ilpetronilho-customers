import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CapitalizePipe } from "../capitalize.pipe";
import { TrimPipe } from "../trim.pipe";

@NgModule({
  declarations: [CapitalizePipe, TrimPipe],
  imports: [CommonModule],
  exports: [CapitalizePipe, TrimPipe]
})
export class PipeModule {}
