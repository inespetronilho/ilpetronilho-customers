import { Component, OnInit, Input } from "@angular/core";
import { ICustomer } from "src/app/shared/interfaces";

@Component({
  selector: "app-costumers-card",
  templateUrl: "./costumers-card.component.html",
  styleUrls: ["./costumers-card.component.scss"]
})
export class CostumersCardComponent implements OnInit {
  constructor() {}
  @Input() customers: ICustomer[] = [];

  ngOnInit() {}
}
