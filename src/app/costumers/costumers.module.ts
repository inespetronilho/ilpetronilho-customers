import { CostumersComponent } from "./costumers.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ClarityModule } from "@clr/angular";

import { CostumersRoutingModule } from "./costumers-routing.module";
import { CostumerModule } from "../costumer/costumer.module";
import { CostumersCardComponent } from "./costumers-card/costumers-card.component";
import { PipeModule } from "../pipes/pipe/pipe.module";

@NgModule({
  declarations: [CostumersComponent, CostumersCardComponent],
  imports: [
    CommonModule,
    CostumersRoutingModule,
    ClarityModule,
    CostumerModule,
    PipeModule
  ]
})
export class CostumersModule {}
