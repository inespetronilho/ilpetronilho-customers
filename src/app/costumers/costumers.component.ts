import { CustomerService } from "./../services/customer.service";
import { ICustomer } from "./../shared/interfaces";
import { CostumerComponent } from "./../costumer/costumer.component";
import { Component, OnInit, Input } from "@angular/core";
import { Observable } from "rxjs";

@Component({
  selector: "app-costumers",
  templateUrl: "./costumers.component.html",
  styleUrls: ["./costumers.component.scss"]
})
export class CostumersComponent implements OnInit {
  constructor(private customerService: CustomerService) {}

  @Input() customersList: ICustomer[] = [];

  ngOnInit() {
    this.populateWithMockData();
  }

  populateWithMockData() {
    const costumersObservable = this.customerService.getCustomers();

    costumersObservable.subscribe((customersData: ICustomer[]) => {
      this.customersList = customersData;
    });
  }
}
