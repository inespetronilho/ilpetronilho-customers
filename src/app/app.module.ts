import { MockDataService } from "./shared/mock";
import { HeaderComponent } from "./header/header.component";
import { CostumersModule } from "./costumers/costumers.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ClarityModule } from "@clr/angular";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { PipeModule } from "./pipes/pipe/pipe.module";

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    CostumersModule
  ],
  providers: [MockDataService],
  bootstrap: [AppComponent],
  declarations: [AppComponent, HeaderComponent]
})
export class AppModule {}
